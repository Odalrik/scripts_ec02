# Explications

Petit fichier pour expliquer la manière dont j'ai utilisé les scripts.

Déjà, il y a au moins 1 script par sujet de TP (TP1, TP2 et TP3, le TP4 ne nécessitait pas de script).

---
---
> Important : Les fichiers de données sont tous sous un nom du type "Tp2_2_1_2_0.csv", pour dire qu'il s'agit du TP2, Partie 2, sous-partie 1, sous-sous-partie 2, paragraphe 0. Oui, Carton rentre dans beaucoup de sous-parties.

> Désolé pour la piètre qualité du code, je découvrais un peu tout ça à ce moment là (l'IDE, Python, les TP à la con où on a pas le temps de tout faire)
---
---

---
## TP1
Cas particulier, car il y a avait une préparation à faire, et j'ai fait 2 scripts supplémentaires : 

### Script_TP1_Preparation
Il ne nécessite pas de fichier de données, il enregistre simplement les courbes résultantes des calculs dans le dossier actuel, avec le nom spécifié.

### Script_TP1_1
Petite foirade fainéante de ma part : les fichiers de données sont récupérés dans le dossier actuel, aucun path n'est à rentrer. Les courbes sont enregistrées dans les dossiers "Manuel" et "Automatique" en fonction des courbes tracées.

### Script_TP1_2
Je commençais à m'organiser : je vais récupérer tous les fichiers de données dans le dossier "data_simulation" à partir du dossier actuel. Je vais ensuite enregistrer toutes les courbes dans le dossier "Simulation".

---
## TP2
Je vais récupérer les données dans 2 dossiers : "TP2_carton_datas" et "TP2_carton_datas3", mais dans mes dossiers, j'ai également "TP2_carton_datas2", donc j'ai dû remplacer le "TP2_carton_datas2" par "TP2_carton_datas3" et relancer l'exécution de la partie concernée.

---
## TP3
Désolé, pour celui-ci j'avais récupéré les graphiques directement sur plecs, j'avais eu un problème et je n'avais pas pu faire comme pour les autres TP. Je n'ai que le script pour générer les courbes de la partie préparation.